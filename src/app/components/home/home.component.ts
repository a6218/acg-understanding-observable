import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  private subscription!: Subscription;
  private ownSubscription!: Subscription;

  ngOnInit(): void {
    // this.subscription = interval(1000).subscribe((count) => {
    //   console.log(count);
    // });

    let ownObservable = new Observable<number>(observer => {
      let count = 0;
      setInterval(() => {
        observer.next(count);
        if (count > 3) {
          observer.error(new Error('Number is greater than 3'));
        }

        if (count === 5) {
          observer.complete();
        }

        count++
      }, 1000);
    });

    this.ownSubscription = ownObservable
    .pipe(
      filter((num: number) => num > 0),
      map((num: number) => {
        return 'Rounded: ' + num;
      }),
    )
    .subscribe({
      next: (count) => {
        console.log(count);
      },
      error: (error) => {
        console.log(error);
      },
      complete: () => {
        console.log('done');
      }
    });
  }

  ngOnDestroy(): void {
    // this.subscription.unsubscribe();
    this.ownSubscription.unsubscribe();
  }
}
