import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {
  activated = false;
  private subscription = new Subscription();

  constructor(private userService: UserService) {
    this.subscription = this.userService.activatedSubject.subscribe(
      result => this.activated = result
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
